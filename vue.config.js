module.exports = {
    chainWebpack: config => {
        config
            .plugin('webpack-bundle-analyzer')
            .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
    },
    chainWebpack: config => {
        // 压缩代码
        config.optimization.minimize(true)
        // 分割代码
        config.optimization.splitChunks({
            chunks: 'all'
        })
        // 用cdn方式引入
        config.externals({
            'vue': 'Vue',
            'element-ui': 'ELEMENT'
        })
    },
    devServer: {
        proxy: {
            '/api': {
                target: 'https://admin.raz-kid.cn',
                ws: true,
                changeOrigin: true
            },
        }
    }
}