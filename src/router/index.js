import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name:'Index',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
    redirect:{
      name:'Menu'
    },
    children : [
      // 首页
      {
        path: 'home/menu',
        name: 'Menu',
        component: () => import(/* webpackChunkName: "users" */ '../views/Menu.vue'),
      },
      // 商品
      {
        path: 'home/management',
        name: 'Management',
        component: () => import(/* webpackChunkName: "management" */ '../views/Management.vue'),
      },
      {
        // 商品新增
        path: 'home/addgoods',
        name: 'Addgoods',
        component: () => import(/* webpackChunkName: "addgoods" */ '../views/goods/Addgoods.vue'),
      },
      // 商品详情
      {
        path:'home/management/details',
        name: 'Details',
        component: () => import(/* webpackChunkName: "details" */ '../views/goods/Details.vue'),
      },
      // 商品编辑
      {
        path:'home/management/edit',
        name: 'Edit',
        component: () => import(/* webpackChunkName: "details" */ '../views/goods/Edit.vue'),
      },
      // 品类管理
      {
        path: 'home/category',
        name: 'Category',
        component: () => import(/* webpackChunkName: "category" */ '../views/Category.vue'),
      },
      // 查看子分类
      {
        path: 'home/category/subclass',
        name: 'Subclass',
        component: () => import(/* webpackChunkName: "subclass" */ '../views/categorys/Subclass.vue'),
      },
      // 订单
      {
        path: 'home/order',
        name: 'Order',
        component: () => import(/* webpackChunkName: "order" */ '../views/Order.vue'),
      },
      // 订单详情
      {
        path:'home/order/vieworder',
        name: 'ViewOrder',
        component: () => import(/* webpackChunkName: "details" */ '../views/orders/ViewOrder.vue'),
      },
      // 用户管理
      {
        path: 'home/user',
        name: 'User',
        component: () => import(/* webpackChunkName: "user" */ '../views/User.vue'),
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
