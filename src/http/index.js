import axios from 'axios'
import { Message } from 'element-ui'
const instance = axios.create({
  baseURL: process.env.VUE_APP_baseURL
})
const http = (url, data, method = 'GET', params) => {
  return instance({
    url,
    method,
    data,
    params,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  }).then(res => {
    if (res.status >= 200 && res.status < 300 || res.status === 304) {
      if (res.data.status === 0) {
        return res.data
      } else {
        Message({
          message: res.data.msg,
          type: 'error'
        })
        Promise.reject(res.data)
      }
    } else {
      Message({
        message: res.statusText,
        type: 'error'
      })
    }
  })
}
export default http