// import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css'
// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
import BreadCrumb from './components/BreadCrumb'
import instance from './http'
Vue.component('bread-crumb',BreadCrumb)
// Vue.use(ElementUI);
Vue.config.productionTip = false
Vue.prototype.$axios=instance
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
